import HeroComponent from "./components/HeroComponent";
import LeftSideComponent from "./components/LeftSideComponent";
import SideBarComponent from "./components/SideBarComponent";

function App() {
  return (
    <div className="grid grid-cols-12">

      <div className="col-span-1">
        <SideBarComponent />
      </div>
      <div className="col-span-9">
        <HeroComponent />
      </div>
      <div className="col-span-3">
        <LeftSideComponent />
      </div>
    </div>
  );
}

export default App;
