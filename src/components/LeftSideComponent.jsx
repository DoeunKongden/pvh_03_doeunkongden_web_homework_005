import React from 'react'

function LeftSideComponent() {
  return (
    <div className='bg-[url(https://cdn.dribbble.com/users/727440/screenshots/6244299/camping.gif)] bg-no-repeat bg-cover bg-center h-[100vh] w-[20vw] fixed top-0 right-0'>
      <ul class="flex justify-end my-12 mx-12 space-x-7">
        <li className='ring-white hover:ring-4 hover:rounded-3xl'><img className='w-7 ' src={require('../assets/notification.png')} alt="" /></li>
        <li><img className='w-7 ring-white hover:ring-4 hover:rounded-3xl' src={require('../assets/comment.png')} alt="" /></li>
        <li><img className='w-8 h-8 rounded-[100%] ring-white hover:ring-4 hover:rounded-3xl' src={require('../assets/lachlan.jpg')} alt="" /></li>
      </ul>

      <div className='flex justify-end'>
        <button type="button" class="mx-12 my-2  text-black bg-yellow-100 hover:bg-yellow-500 hover:text-white  font-medium rounded-lg text-sm px-5 py-2.5">My Amazing Trip</button>
      </div>

      <h1 className='text-white text-3xl my-8 mx-14'>I Like Laying Down On The Sand And Look At The Moon</h1>

      <h1 className='text-white  text-xl mx-14 mt-24'>27 People are going to this trip</h1>

      <ul class="flex mx-14 my-6 space-x-7">
        <li><img className='w-9 h-9 rounded-[100%] ring-4 ring-white' src={require('../assets/lachlan.jpg')} alt="" /></li>
        <li><img className='w-9 h-9 rounded-[100%] ring-4 ring-white' src={require('../assets/raamin.jpg')} alt="" /></li>
        <li><img className='w-9 h-9 rounded-[100%] ring-4 ring-orange-600' src={require('../assets/nonamesontheway.jpg')} alt="" /></li>
        <li><div className='w-9 h-9 bg-yellow-100 rounded-[100%] items-center flex justify-center ring-4 ring-yellow-500'>23+</div></li>
      </ul>


    </div>
  )
}

export default LeftSideComponent