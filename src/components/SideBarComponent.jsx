import React from 'react'

function SideBarComponent() {
  return (
    <div>
      <aside className="fixed flex flex-col top-0 left-0 z-40 w-[8vw] h-screen transition-transform -translate-x-full sm:translate-x-0">
        {/* first list item */}
        <div className="h-32 px-3 flex  justify-center py-10 overflow-y-hidden bg-slate-200">
          <ul className="space-y-5 ">
            <li><img className='w-8 ' src={require('../assets/category_icon.png')} alt="" /></li>
          </ul>
        </div>

        {/*  second list item */}
        <div class="h-full flex justify-center px-3 py-10 overflow-y-auto bg-slate-200">
          <ul class="space-y-7">
            <li><img className='w-8 ' src={require('../assets/cube.png')} alt="" /></li>
            <li><img className='w-8 ' src={require('../assets/list.png')} alt="" /></li>
            <li><img className='w-8 ' src={require('../assets/messenger.png')} alt="" /></li>
            <li><img className='w-8 ' src={require('../assets/list.png')} alt="" /></li>
          </ul>
        </div>

        {/*  second list item */}
        <div class="h-full flex justify-center px-3 py-4 overflow-y-auto bg-slate-200">
          <ul class="space-y-7">
            <li><img className='w-8 ' src={require('../assets/success.png')} alt="" /></li>
            <li><img className='w-8 ' src={require('../assets/security.png')} alt="" /></li>
            <li><img className='w-8 ' src={require('../assets/users.png')} alt="" /></li>
          </ul>
        </div>

        {/* third list item */}
        <div className="h-full flex justify-center px-3 py-4 overflow-y-auto bg-slate-200">
          <ul className="space-y-7">
            <li><img className='w-8 h-8 rounded-[100%] ' src={require('../assets/lachlan.jpg')} alt="" /></li>
            <li><img className='w-8 h-8 rounded-[100%] ' src={require('../assets/raamin.jpg')} alt="" /></li>
            <li><img className='w-8 h-8 rounded-[100%] ' src={require('../assets/nonamesontheway.jpg')} alt="" /></li>
            <li><img className='w-8 h-8 rounded-[100%] ' src={require('../assets/plus.png')} alt="" /></li>
          </ul>
        </div>


      </aside>
    </div>
  )
}

export default SideBarComponent