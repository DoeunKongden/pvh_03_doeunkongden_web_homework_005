import React, { useEffect, useState } from 'react'

function FormComponent({ showModal, onClose, setInfo, info, setShowModal }) {

  const [newObj, setNewObj] = useState([])


  const handleChange = (e) => {
    // console.log(e.target.value);
    // console.log(e.target.value);
    setNewObj({ ...newObj, [e.target.name]: e.target.value })
  }
  useEffect(() => {
    console.log(newObj);
  }, [newObj])


  const handleSubmit = (e) => {
    e.preventDefault()
    console.log(newObj);

    setInfo([...info, newObj])

    console.log(info);
    setShowModal(false)
  }

  if (!showModal) {
    return null;
  }

  return (

    <div className="duration-200 flex justify-center items-center fixed z-50  w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-[calc(100%-1rem)] md:h-full backdrop-blur-[2px] bg-black/30">
      <div className="relative w-full h-full max-w-2xl md:h-auto">

        <div className="relative bg-white rounded-lg shadow ">

          <div className="flex items-start justify-between p-4 border-b rounded-t ">
            <h3 className="text-xl font-semibold text-gray-900">
              Add New Trip
            </h3>
            <button type="button" onClick={onClose} className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center " data-modal-hide="defaultModal">
              <svg className="w-5 h-5" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clipRule="evenodd"></path></svg>
              <span class="sr-only">Close modal</span>
            </button>
          </div>

          <div className="p-6 space-y-6">
            <div className="mb-6">
              <label className="block mb-2 text-sm font-medium text-gray-900">Title</label>
              <input onChange={handleChange} name='title' type="text" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 " placeholder="Sihanouk Ville" required />
            </div>

            <div className="mb-6">
              <label className="block mb-2 text-sm font-medium text-gray-900">Description</label>
              <input onChange={handleChange} name='description' type="text" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="Happy Place With Sunset And Beach" required />
            </div>

            <div className="mb-6">
              <label className="block mb-2 text-sm font-medium text-gray-900">People Going</label>
              <input onChange={handleChange} name='peopleGoing' type="text" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="3200" required />
            </div>

            <label className="block mb-2 text-sm font-medium text-gray-900">Type Of Adventure</label>
            <select name='status' onChange={handleChange} className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 ">
              <option selected hidden >---Choose any option---</option>
              <option value="beach">Beach</option>
              <option value="mountain">Moutain</option>
              <option value="forest">Forest</option>
            </select>


          </div>
          <div className="flex items-center p-6 space-x-2 border-t border-gray-200 rounded-b dark:border-gray-600">
            <button type="button" onClick={handleSubmit} className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center ">Submit</button>
            <button type="submit" onClick={onClose} className="text-gray-500 bg-white hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-blue-300 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 ">Close</button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default FormComponent