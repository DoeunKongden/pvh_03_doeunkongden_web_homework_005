import React, { useEffect, useState } from 'react'
import MyCardComponent from './MyCardComponent'
import FormComponent from './FormComponent'
import SkeletonComponent from './SkeletonComponent';

function HeroComponent() {

  const [showModal, setShowModal] = useState(false);
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {

    setTimeout(() => {
      console.log("first")
      setIsLoading(false)
    }, 5000)
  }, [])


  const [info, setInfo] = useState([
    {
      id: 1,
      title: "koh kong krav",
      description:
        "Koh Kong Krav Beach is in the 5th place out of 13 beaches in the Koh Kong region The beach is located in a natural place, among the mountains. It is partially covered 	   by trees which give natural shade. It is a spacious coastline with crystal turquoise water and white fine sand, so you don't need special shoes.",
      status: "beach",
      peopleGoing: "1537",
    },
    {
      id: 2,
      title: "phnom sampov",
      description:
        " This legendary 100 metres high mountain, topped by Wat Sampeou, contains 3 natural caves, lined with Buddhist shrines and statues: Pkasla, Lakhaon and Aksopheak.",
      status: "mountain",
      peopleGoing: "81000",
    },
    {
      id: 3,
      title: "kirirom",
      description:
        "Kirirom National Park, a high altitude plateau, is known for its unique high elevation pine forest, which forms the headwaters for numerous streams feeding Kampong 	   	   Speu Town.",
      status: "forest",
      peopleGoing: "2500",
    },
    {
      id: 4,
      title: "kirirom",
      description:
        "Kirirom National Park, a high altitude plateau, is known for its unique high elevation pine forest, which forms the headwaters for numerous streams feeding Kampong Speu Town.",
      status: "forest",
      peopleGoing: "2500",
    },

  ])

  return (
    <div className='h-screen '>
      <div className='flex justify-between'>
        <h1 className='text-4xl font-semibold my-20 mx-10'>Good Evening Team!</h1>
        <button onClick={() => { setShowModal(true) }} class="w-40 h-14 my-20 mr-24 block text-white bg-gray-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center " type="button">
          Add New Trip
        </button>
      </div>

      <FormComponent setInfo={setInfo} info={info} setShowModal={setShowModal} showModal={showModal} onClose={() => setShowModal(false)} />

      {
        !isLoading ? (
          <div className=''>
            <MyCardComponent informations={info} setInfo={setInfo} />
          </div>
        ) : (
          <div className='flex justify-center items-center h-96'>
            <SkeletonComponent />
          </div>
        )
      }

    </div>
  )
}

export default HeroComponent