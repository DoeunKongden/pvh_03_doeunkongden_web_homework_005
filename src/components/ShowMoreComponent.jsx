import React from 'react'

function ShowMoreComponent({ showMore, onClose }) {
  if (!showMore) {
    return null;
  }
  return (
    <div>
      show more component
      <button onClick={onClose}>Close</button>
    </div>
  )
}

export default ShowMoreComponent