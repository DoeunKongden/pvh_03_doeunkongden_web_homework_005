import React, { useState } from 'react'

function MyCardComponent({ informations, setInfo }) {

  const [showMore, setShowMore] = useState(false);
  const [dialogData, setDialogData] = useState({})

  const handleChangeStatus = (data) => {
    setInfo((prevData) =>
      prevData.map((item) => {
        if (item.id === data) {
          if (item.status === "beach") {
            return { ...item, status: "mountain" }
          } else if (item.status === "mountain") {
            return { ...item, status: "forest" }
          } else {
            return { ...item, status: "beach" }
          }
        }
        return item
      })
    )
  }

  return (
    <div className='flex justify-center items-center'>
      <div className='grid grid-cols-3 gap-10'>
        {informations.map((info, index) => {
          return (
            <div key={index} className=' h-[400px] '>
              < div className="max-w-sm p-10  my-4 bg-gray-700 border border-gray-200 rounded-lg shadow " >
                <h5 className="mb-2 text-2xl font-bold tracking-tight text-white ">{info.title}</h5>
                <p className="mb-3 font-normal text-white line-clamp-3">{info.description}</p>
                <h4 className='text-xl tracking-light text-white my-4'>{info.peopleGoing} : People Are Going</h4>
                <div className='flex gap-4'>
                  <button type="button" onClick={() => handleChangeStatus(info.id)} className={info.status === "beach" ? 'text-white w-32 bg-blue-700 hover:bg-blue-400 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2' : info.status === "mountain" ? 'text-white w-32 bg-gray-800 hover:bg-gray-400 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2' : info.status === "forest" ? 'text-white w-32 bg-green-600 hover:bg-green-400 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2' : 'text-white w-32 bg-blue-700 hover:bg-blue-400 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2'}>{info.status}</button>
                  <button type="button" onClick={() => {
                    setShowMore(true, console.log(index))
                    setDialogData(info)
                  }} className="text-white bg-gray-400 hover:bg-gray-200 hover:text-black h focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2">Read More</button>
                </div>
              </div>
              {/*           
            <ShowMoreComponent showMore={showMore} onClose={() => { setShowMore(false) }} /> */}

              {showMore && (
                <div className="flex justify-center items-center fixed z-50  w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-[calc(100%-1rem)] md:h-full backdrop-blur-[2px] bg-black/30">
                  <div className="relative w-full h-full max-w-2xl md:h-auto">

                    <div className="relative bg-white rounded-lg shadow dark:bg-gray-700">

                      <div className="flex items-start justify-between p-4 border-b rounded-t dark:border-gray-600">
                        <h3 className="text-xl font-semibold text-gray-900 dark:text-white">
                          Read More
                        </h3>
                        <button type="button" onClick={() => { setShowMore(false) }} className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white" data-modal-hide="defaultModal">
                          <svg aria-hidden="true" className="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                          <span className="sr-only">Close modal</span>
                        </button>
                      </div>

                      <div key={dialogData.id} className="p-6 space-y-6">
                        {dialogData.description}
                      </div>
                    </div>
                  </div>
                </div>

              )}
            </div>

          )
        })}
      </div >
    </div>
  )
}

export default MyCardComponent